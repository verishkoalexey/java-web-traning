package by.traning.module0;

public class Cycles {
    public static void main(String[] args) {
        task6(3);
    }
    private static void task1(){
        int i;
        for (i = 1; i <= 5; i++){
            System.out.println(i);
        }
    }

    private static void task2(){
        int i;
        for (i = 5; i >= 1; i--){
            System.out.println(i);
        }
    }

    private static void task3(){
        int i;
        for (i = 3; i <= 10; i++){
            System.out.println("3*" + i + "=" + 3*i);
        }
    }

    private static void task4(){
        int i = 0;
        while ( i < 100 ){
            i=i+2;
            System.out.println(i);
        }
    }

    private static void task5(){
        int i = 1;
        int sum = 0;
        while (i <= 99) {
            sum += i;
            i += 2;
        }
        System.out.println("Сумма нечетных чисел равна " + sum);
        System.out.println();
    }
    private static void task6(int num) {
        int i = 1;
        int sum = 0;
        while (i <= num) {
            sum += i;
            i++;
        }
        System.out.println("Сумма чисел от 1 до " + num + " равна " + sum);
    }

}

