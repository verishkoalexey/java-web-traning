package by.traning.module0.aggregationComposition.task2.service.impl;

import by.traning.module0.aggregationComposition.task2.model.Car;
import by.traning.module0.aggregationComposition.task2.model.Engine;
import by.traning.module0.aggregationComposition.task2.model.Wheel;
import by.traning.module0.aggregationComposition.task2.service.CarService;

import java.util.ArrayList;
import java.util.List;

public class CarServiceImpl implements CarService {

    public Engine getEngine() {
        return new Engine();
    }

    public void setNumber(Car car, String number) {
        car.setNumber(number);
    }

    public void startEngine(Car car) {
        car.getEngine().setRun(true);
    }

    public void addFuel(Car car) {
        car.setFuel(car.getFuel() + 10);
    }

    public void stopEngine(Car car) {
        car.getEngine().setRun(false);
    }

    public void setNewWheelsList(Car car) {
        List<Wheel> wheelList = new ArrayList<>();
        for (int i = 1; i < 5; i++) {
            wheelList.add(new Wheel("Колесо" + (int) (Math.random() * 10)));
        }
        car.setWheelList(wheelList);
    }

    public List<Wheel> getNewWheelsList() {
        List<Wheel> wheelList = new ArrayList<>();
        for (int i = 1; i < 5; i++) {
            wheelList.add(new Wheel("Колесо" + (int) (Math.random() * 10)));
        }
        return wheelList;
    }

    public void go(Car car) {
        if(!car.getEngine().isRun()){
            System.out.println("Start engine");
        }
    }
}
