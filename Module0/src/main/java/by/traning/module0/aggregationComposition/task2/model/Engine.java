package by.traning.module0.aggregationComposition.task2.model;

public class Engine {
    private boolean isRun;

    @Override
    public String toString() {
        return "Engine{" +
                "isRun=" + isRun +
                '}';
    }

    public boolean isRun() {
        return isRun;
    }

    public void setRun(boolean run) {
        isRun = run;
    }
}
