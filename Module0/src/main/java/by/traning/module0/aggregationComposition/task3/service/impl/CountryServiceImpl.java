package by.traning.module0.aggregationComposition.task3.service.impl;

import by.traning.module0.aggregationComposition.task3.model.City;
import by.traning.module0.aggregationComposition.task3.model.Country;
import by.traning.module0.aggregationComposition.task3.model.District;
import by.traning.module0.aggregationComposition.task3.model.Region;
import by.traning.module0.aggregationComposition.task3.service.CountryService;

public class CountryServiceImpl implements CountryService {
    @Override
    public int getNumberOfRegions(Country country) {
        return country.getRegionList().size();
    }

    @Override
    public double getSquare(Country country) {
        double square = 0;
        for (Region region : country.getRegionList()) {
            for (District district : region.getDistrictList()) {
                square += district.getSquare();
            }
        }
        return square;
    }

    @Override
    public void addCityInDistrict(City city, District district) {
        district.getCitiesList().add(city);
    }

    @Override
    public void addDistrictInRegion(District district, Region region) {
        region.getDistrictList().add(district);
    }

    @Override
    public void addRegionInCountry(Region region, Country country) {
        country.getRegionList().add(region);
    }

    @Override
    public void setRegionCapital(Region region, City name) {
        region.setRegionCapital(name);
    }

    @Override
    public City getRegionCapital(Region region) {
        return region.getRegionCapital();
    }
}
