package by.traning.module0.aggregationComposition.task2.service;

import by.traning.module0.aggregationComposition.task2.model.Car;
import by.traning.module0.aggregationComposition.task2.model.Engine;
import by.traning.module0.aggregationComposition.task2.model.Wheel;

import java.util.List;

public interface CarService {
    Engine getEngine();
    void setNumber(Car car, String number);
    void startEngine(Car car);
    void addFuel(Car car);
    void stopEngine(Car car);
    void setNewWheelsList(Car car);
    List<Wheel> getNewWheelsList();
    void go(Car car);
}
