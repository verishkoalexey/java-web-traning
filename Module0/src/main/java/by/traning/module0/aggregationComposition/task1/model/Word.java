package by.traning.module0.aggregationComposition.task1.model;

public class Word {

    private String word;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public Word(String word) {
        this.word = word;
    }
}
