package by.traning.module0.aggregationComposition.task1;

import by.traning.module0.aggregationComposition.task1.model.Sentence;
import by.traning.module0.aggregationComposition.task1.model.Text;
import by.traning.module0.aggregationComposition.task1.model.Word;
import by.traning.module0.aggregationComposition.task1.service.TextService;
import by.traning.module0.aggregationComposition.task1.service.TextServiceImpl;

public class Application {
    public static void main(String[] args) {
        TextService textService = new TextServiceImpl();
        Word word1 = new Word("Hello");
        Word word2 = new Word("World");
        Word word3 = new Word("!");

        Sentence sentence1 = new Sentence();
        textService.addWordToSentence(sentence1,word1);

        Sentence sentence2 = new Sentence();
        textService.addWordToSentence(sentence2,word2);
        textService.addWordToSentence(sentence2,word3);

        Text text = new Text();
        textService.addSentenceToText(text,sentence1);
        textService.addSentenceToText(text,sentence2);
        textService.setHeader(text,"Header");

        System.out.println(text);


    }
}
