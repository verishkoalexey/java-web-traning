package by.traning.module0.aggregationComposition.task1.service;

import by.traning.module0.aggregationComposition.task1.model.Sentence;
import by.traning.module0.aggregationComposition.task1.model.Text;
import by.traning.module0.aggregationComposition.task1.model.Word;

import java.util.ArrayList;

public class TextServiceImpl implements TextService {

    public String getHeader(Text text) {
        if (!text.getTextName().equals("")) {
            return "<h1>" + text.getTextName() + "</h1>";
        }
        else return "";
    }

    public void setHeader(Text text, String textName) {
        text.setTextName(textName);
    }

    public void addSentenceToText(Text text, Sentence sentence) {
        text.getSentences().add(sentence);
    }

    public void addWordToSentence(Sentence sentence, Word word) {
        sentence.getWords().add(word);
    }
}
