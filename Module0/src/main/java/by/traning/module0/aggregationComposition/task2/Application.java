package by.traning.module0.aggregationComposition.task2;

import by.traning.module0.aggregationComposition.task2.model.Car;
import by.traning.module0.aggregationComposition.task2.model.Engine;
import by.traning.module0.aggregationComposition.task2.model.Wheel;
import by.traning.module0.aggregationComposition.task2.service.CarService;
import by.traning.module0.aggregationComposition.task2.service.impl.CarServiceImpl;

import java.util.List;

public class Application {
    public static void main(String[] args) {
        CarService carService = new CarServiceImpl();
        Engine engine = carService.getEngine();
        List<Wheel> wheels = carService.getNewWheelsList();

        Car audi = new Car(engine,"IS-7 8647", wheels,5);

        carService.setNewWheelsList(audi);
        carService.setNumber(audi,"IS-7 7788");

        carService.addFuel(audi);
        carService.startEngine(audi);
        carService.go(audi);

    }
}
