package by.traning.module0.aggregationComposition.task3.service;

import by.traning.module0.aggregationComposition.task3.model.City;
import by.traning.module0.aggregationComposition.task3.model.Country;
import by.traning.module0.aggregationComposition.task3.model.District;
import by.traning.module0.aggregationComposition.task3.model.Region;

public interface CountryService {
    int getNumberOfRegions(Country country);
    double getSquare(Country country);
    void addCityInDistrict(City city, District district);
    void addDistrictInRegion(District district, Region region);
    void addRegionInCountry(Region region, Country country);
    void setRegionCapital(Region region, City name);
    City getRegionCapital(Region region);

}
