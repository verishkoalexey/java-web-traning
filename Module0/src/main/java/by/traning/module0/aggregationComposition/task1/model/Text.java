package by.traning.module0.aggregationComposition.task1.model;

import java.util.ArrayList;

public class Text {
    private String textName = "";
    private ArrayList<Sentence> sentences;

    public Text(){

    }

    public Text(String textName, ArrayList<Sentence> sentences) {
        this.textName = textName;
        this.sentences = sentences;
    }

    public String getTextName() {
        return textName;
    }

    public void setTextName(String textName) {
        this.textName = textName;
    }

    public ArrayList<Sentence> getSentences() {
        return sentences;
    }

    public void setSentences(ArrayList<Sentence> sentences) {
        this.sentences = sentences;
    }
}
