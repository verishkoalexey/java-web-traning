package by.traning.module0.aggregationComposition.task3.model;

import java.util.ArrayList;
import java.util.List;

public class Country {
    City city;
    List<Region> regionList;

    public Country(City city) {
        this.city = city;
    }

    public City getCity() {
        return city;
    }

    public void setCity(City city) {
        this.city = city;
    }

    public List<Region> getRegionList() {
        return regionList;
    }

    public void setRegionList(List<Region> regionList) {
        this.regionList = regionList;
    }

    @Override
    public String toString() {
        return "Country{" +
                "city=" + city +
                ", regionList=" + regionList +
                '}';
    }
}
