package by.traning.module0.aggregationComposition.task3.model;

import java.util.ArrayList;
import java.util.List;

public class District {
    String districtName;
    List<City> citiesList = new ArrayList<>();
    double square;

    public District(String districtName,int square) {
        this.districtName = districtName;
        this.square = square;
    }

    public String getDistrictName() {
        return districtName;
    }

    public void setDistrictName(String districtName) {
        this.districtName = districtName;
    }

    public List<City> getCitiesList() {
        return citiesList;
    }

    public void setCitiesList(List<City> citiesList) {
        this.citiesList = citiesList;
    }

    public double getSquare() {
        return square;
    }

    public void setSquare(double square) {
        this.square = square;
    }

    @Override
    public String toString() {
        return "District{" +
                "districtName='" + districtName + '\'' +
                ", citiesList=" + citiesList +
                ", square=" + square +
                '}';
    }
}
