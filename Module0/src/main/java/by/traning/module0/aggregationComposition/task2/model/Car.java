package by.traning.module0.aggregationComposition.task2.model;

import java.util.List;

public class Car {
    private Engine engine;
    private String number;
    private List<Wheel> wheelList;
    private int fuel = 0;

    @Override
    public String toString() {
        return "Car{" +
                "engine=" + engine +
                ", number='" + number + '\'' +
                ", wheelList=" + wheelList +
                ", fuel=" + fuel +
                '}';
    }

    public Car(Engine engine, String number, List<Wheel> wheelList, int fuel) {
        this.engine = engine;
        this.number = number;
        this.wheelList = wheelList;
        this.fuel = fuel;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public List<Wheel> getWheelList() {
        return wheelList;
    }

    public void setWheelList(List<Wheel> wheelList) {
        this.wheelList = wheelList;
    }

    public int getFuel() {
        return fuel;
    }

    public void setFuel(int fuel) {
        this.fuel = fuel;
    }
}
