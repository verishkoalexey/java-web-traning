package by.traning.module0.aggregationComposition.task1.service;

import by.traning.module0.aggregationComposition.task1.model.Sentence;
import by.traning.module0.aggregationComposition.task1.model.Text;
import by.traning.module0.aggregationComposition.task1.model.Word;

public interface TextService {
    String getHeader(Text text);
    void setHeader(Text text, String textName);
    void addSentenceToText(Text text, Sentence sentence);
    void addWordToSentence(Sentence sentence, Word word);
}
