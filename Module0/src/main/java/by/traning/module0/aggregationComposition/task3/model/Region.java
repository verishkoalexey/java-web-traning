package by.traning.module0.aggregationComposition.task3.model;

import java.util.ArrayList;
import java.util.List;

public class Region {
    String name;
    List<District> districtList = new ArrayList<>();
    City regionCapital;

    public Region(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<District> getDistrictList() {
        return districtList;
    }

    public void setDistrictList(List<District> districtList) {
        this.districtList = districtList;
    }

    public City getRegionCapital() {
        return regionCapital;
    }

    public void setRegionCapital(City regionCapital) {
        this.regionCapital = regionCapital;
    }

    @Override
    public String toString() {
        return "Region{" +
                "name='" + name + '\'' +
                ", districtList=" + districtList +
                ", regionCapital=" + regionCapital +
                '}';
    }
}
