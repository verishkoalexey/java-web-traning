package by.traning.module0;
import java.util.Arrays;

public class Array {

    public static void main(String[] args) {

    }

    private static void task1(int[] arr, int k) {
        int sum = 0;
        for (int val : arr) {
            if (val % k == 0) {
                sum += val;
            }
        }
        System.out.println(sum);
    }

    private static void task3(int[] arr) {
        for (int val : arr) {
            if (val < 0) {
                System.out.println("Отрицательное");
                break;
            } else if (val > 0) {
                System.out.println("Положительное");
                break;
            }
        }
    }

    private static void task4(int[] arr) {
        int a = arr[0];
        boolean up = true;
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < a) {
                up = false;
                break;
            }
            a = arr[i];
        }
        if (up) {
            System.out.println("Возрастающая последовательность");
        } else {
            System.out.println("Не возрастающая последовательность");
        }
    }

    private static void task5(int[] arr) {
        int count = 0;
        for (int val : arr) {
            if (val % 2 == 0) {
                count++;
            }
        }
        if (count != 0) {
            int[] finalArr = new int[count];
            for (int val : arr) {
                if (val % 2 == 0) {
                    finalArr[finalArr.length - count] = val;
                    count--;
                }
            }
            System.out.println(Arrays.toString(finalArr));
        } else
            System.out.println("Четных чисел нет");
    }

    private static void task6(int[] arr) {
        int maxItem = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > maxItem) {
                maxItem = arr[i];
            }
        }

        int minItem = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < minItem) {
                minItem = arr[i];
            }
        }
        System.out.println(maxItem - minItem);
    }

    private static void task7(double z, double[] arr) {
        int count = 0;
        for (int i = 0; i < 10; i++) {
            if (arr[i] > z) {
                arr[i] = z;
                count++;
            }
        }
        System.out.println("Заменено = " + count);
    }

    private static void task8(int[] arr) {
        int cPos = 0;
        int cNeg = 0;
        int cZero = 0;
        for (int val : arr) {
            if (val > 0) {
                cPos++;
            } else if (val < 0) {
                cNeg++;
            } else cZero++;
        }
        System.out.println("Положительных элементов = " + cPos);
        System.out.println("Отрицательных элементов = " + cNeg);
        System.out.println("Нулевых элементов = " + cZero);
    }

    private static void task9(int[] arr) {
        int maxNum = 0;
        int minNum = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > arr[maxNum]) {
                maxNum = i;
            }
            if (arr[i] < arr[minNum]) {
                minNum = i;
            }
        }
        int a = arr[minNum];
        arr[minNum] = arr[maxNum];
        arr[maxNum] = a;

        System.out.println(Arrays.toString(arr));
    }

    private static void task10(int i, int[] arr) {
        for (int val : arr) {
            if (val > i) {
                System.out.print(val + " ");
            }
        }
        System.out.println();
    }

    private static void task12(double[] arr) {
        double sum = 0;
        for (double v : arr) {
            boolean isEasy = true;
            for (int i = 2; i < Math.abs(v); i++) {
                if (v % i == 0) {
                    isEasy = false;
                    break;
                }
            }
            if (isEasy && v != 0) {
                sum += v;
            }
        }
        System.out.println("Сумма чисел равна = " + sum);
    }

    private static void task13(int m, int l, int n, int[] arr) {
        int count = 0;
        for (int val : arr) {
            if (val % m == 0 && val > l && val < n) {
                count++;
            }
        }
        System.out.println(count);
    }

    private static void task14(int[] arr) {
        int minElem = arr[0];
        int maxElem = arr[0];
        for (int i = 0; i < arr.length; i++) {
            if (i % 2 == 0 && arr[i] > maxElem) {
                maxElem = arr[i];
            }
            else
            if (arr[i] <= minElem) {
                minElem = arr[i];
            }
        }
        System.out.println("Максимальное значение из четных номеров = " + maxElem);
        System.out.println("Минимальное значение из нечетных номеров = " + minElem);
    }

    private static void task15(int c, int d, double[] arr) {
        int count = 0;
        for (double v : arr) {
            if (v >= c && v <= d) {
                count++;
            }
        }
        double[] newArr = new double[count];
        for (double v : arr) {
            if (v >= c && v <= d) {
                newArr[newArr.length - count] = v;
                count--;
            }
        }
        if (newArr.length == 0) {
            System.out.println("Не найдено");
        }
        else System.out.println(Arrays.toString(newArr));
    }

}

