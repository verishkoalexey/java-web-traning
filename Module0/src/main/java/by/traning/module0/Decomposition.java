package by.traning.module0;

public class Decomposition {
    public static void main(String[] args) {
        task1(0.0, 0.0, 3.0, 0.0, 0.0, 4.0);
    }

    private static void task1(double x1, double y1, double x2, double y2, double x3, double y3){
        double a = Math.sqrt((x1 - x2) * (x1 - x2) + (y1 - y2) * (y1 - y2));
        double b = Math.sqrt((x1 - x3) * (x1 - x3) + (y1 - y3) * (y1 - y3));
        double c = Math.sqrt((x2 - x3) * (x2 - x3) + (y2 - y3) * (y2 - y3));
        if (a + b <= c || a + c <= b || a + c <= b)
            System.out.println("Треугольник не существует");
        else
        {
            double p = (a + b + c) / 2.0;
            double square = Math.sqrt(p * (p - a) * (p - b) * (p - c));
            System.out.println("Площадь: " + square);
        }
    }

    private static int task2(int a, int b){
        int NOK = (a*b)/NOD(a, b);
        return NOK;
    }
    private static int NOD(int a, int b){
        while (a != b){
            if (a > b){
                a = a - b;
            }
            else{
                b = b - a;
            }
        }
        return a;
    }

    private static void task3(int a, int b, int c, int d){
        int x = NOD(a, b);
        int y = NOD(c, d);
        System.out.println(NOD(x,y));
    }

    private static void task4(int a, int b, int c){
        int x = task2(a, b);
        System.out.println(task2(x, c));
    }

    private static void task5(int a, int b, int c){
        int max;
        int min;
        int sum;
        if(a >= b){
            if(b >= c){
                max = a;
                min = c;
            }else{
                if(a >= c){
                    max = a;
                }else{
                    max = c;
                }
                min = b;
            }
        }else{
            if(b >= c){
                if(a >= c){
                    min = c;
                }else{
                    min = a;
                }
                max = b;
            }else{
                max = c;
                min = a;
            }
        }
        sum = min + max;
        System.out.println("Сумма равна: "+sum);
    }

    private static void task6(double a) {
        double square = Math.sqrt(3) * Math.pow(a, 2) / 4;
        System.out.println(square);
    }

    private static void task8(int[] a) {
        int beforeMax = Integer.MIN_VALUE;
        int max = Integer.MIN_VALUE;

        for (int val : a) {
            if (val > max) {
                max = val;
            }
        }
        for (int val : a) {
            if (val > beforeMax && val < max) {
                beforeMax = val;
            }
        }
        System.out.println(beforeMax);

    }

    private static void task9(int i1, int i2, int i3) {
        if(NOD(i1, i2) == 1 && NOD(i2, i3) == 1 && NOD(i1, i3) == 1){
            System.out.println("Взаимно простые");
        }
        else{
            System.out.println("Не взаимно простые");
        }

    }
    private static void task10() {
        int sum = 0;
        for (int i=1;i<10;i+=2) {
            sum+=factorial(i);
        }
        System.out.println(sum);
    }

    private static int factorial(int n) {
        int factorial = 1;
        for(int i = 1; i <=n; i++){
            factorial*=i;
        }
        return factorial;
    }

}
