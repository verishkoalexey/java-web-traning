package by.traning.module0;

public class Branching {

    public static void main(String[] args) {
        task10(5.0,6.0);
    }
    private static void task1(double a, double b) {
        if (a < b) {
            System.out.println(7);
        } else {
            System.out.println(8);
        }
    }

    private static void task2(double a, double b) {
        if (a < b) {
            System.out.println("yes");
        } else {
            System.out.println("no");
        }
    }

    private static void task3(double a) {
        if (a < 3) {
            System.out.println("yes");
        } else if (a > 3) {
            System.out.println("no");
        }
    }

    private static void task4(double a, double b) {
        if (a == b) {
            System.out.println("yes");
        } else {
            System.out.println("no");
        }
    }

    private static void task5(double a, double b) {
        if (a > b) {
            System.out.println("Наименьшее " + b);
        } else if (a < b) {
            System.out.println("Наименьшее " + a);
        } else {
            System.out.println("Они равны");
        }
    }

    private static void task6(double a, double b) {
        if (a > b) {
            System.out.println("Наибольшее " + a);
        } else if (a < b) {
            System.out.println("Наибольшее " + b);
        } else {
            System.out.println("Они равны");
        }
    }

    private static void task7(double a, double b, double c, double x) {
        double res = Math.abs(a * x * x + b * x + c);
        System.out.println(res);
    }

    private static void task8(int a, int b) {
        if (Math.pow(a, 2) > Math.pow(b, 2)) {
            System.out.println("Наименьший квадрат " + Math.pow(a, 2));
        } else if (Math.pow(a, 2) < Math.pow(b, 2)) {
            System.out.println("Наименьший квадрат " + Math.pow(b, 2));
        } else {
            System.out.println("Они равны" + Math.pow(b, 2));
        }
    }

    private static void task9(double a, double b, double c) {
        if (a > 0 && b > 0 && c > 0) {
            if (a == b && b == c) {
                System.out.println("Треугольник равносторонний");
            } else {
                System.out.println("Треугольник не равносторонний");
            }
        } else {
            System.out.println("Не существует");
        }
    }

    private static void task10(double r1, double r2) {
        double square1 = Math.PI * Math.pow(r1, 2);
        double square2 = Math.PI * Math.pow(r2, 2);
        if (square1 > square2) {
            System.out.println("Второй меньше");
        } else {
            System.out.println("Первый меньше");
        }
    }

}

