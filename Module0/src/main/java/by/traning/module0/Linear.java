package by.traning.module0;

public class Linear {

    public static void main(String[] args) {
        task1(1.0,2.0);
    }

    private static void task1(double x, double y) {
        double sum = x + y;
        double quotient = x - y;
        double composition = x * y;
        double div = x / y;
        System.out.println("Сумма " + sum);
        System.out.println("Разность " + quotient);
        System.out.println("Произведение " + composition);
        System.out.println("Деление " + div);
    }

    private static void task2(double x) {
        double res = x + 3;
        System.out.println(res);
    }

    private static void task3(double x, double y) {
        double res = 2 * x + (y - 2) * 5;
        System.out.println(res);
    }

    private static void task4(double a, double b, double c) {
        double res = (a - 3) * b / 2 + c;
        System.out.println(res);
    }

    private static void task5(double a, double b) {
        double res = (a + b) / 2;
        System.out.println(res);
    }

    private static void task7(double length) {
        double square = length * length / 2;
        System.out.println(square);
    }

    private static void task8(double a, double b, double c) {
        double res = (b + Math.sqrt(Math.pow(b, 2) + 4 * a * c)) / (2 * a) - Math.pow(a, 3) * c + Math.pow(b, -2);
        System.out.println(res);
    }

    private static void task9(double a, double b, double c, double d) {
        double res = a * b / (c * d) - (a * b - c) / (c * d);
        System.out.println(res);
    }

    private static void task11(double a, double b) {
        double hyp = Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
        double square = a * b / 2;
        double per = a + b + hyp;
        System.out.println("Площадь " + square);
        System.out.println("Периметр " + per);
    }

    private static void task12(double x1, double y1, double x2, double y2) {
        double res = Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
        System.out.println(res);
    }

    private static void task15() {
        System.out.println("Степень 1 " + Math.pow(Math.PI, 1));
        System.out.println("Степень 2 " + Math.pow(Math.PI, 2));
        System.out.println("Степень 3 " + Math.pow(Math.PI, 3));
        System.out.println("Степень 4 " + Math.pow(Math.PI, 4));
    }

}

