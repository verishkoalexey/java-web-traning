package by.traning.module0.simpleObjects;

public class Task2 {
    private int x;
    private int y;

    public Task2() {
        x = 5;
        y = 7;
    }

    public Task2(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}
