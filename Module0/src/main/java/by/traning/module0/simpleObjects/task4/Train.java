package by.traning.module0.simpleObjects.task4;

import java.util.Date;

public class Train {
    private String destination;
    private int number;
    private Date timeDepart;

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public Date getTimeDepart() {
        return timeDepart;
    }

    public void setTimeDepart(Date timeDepart) {
        this.timeDepart = timeDepart;
    }

    public Train(String destination, int number, Date timeDepart) {
        this.destination = destination;
        this.number = number;
        this.timeDepart = timeDepart;
    }
}
