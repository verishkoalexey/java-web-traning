package by.traning.module0.simpleObjects;

public class Task1 {
    private int x;
    private int y;

    @Override
    public String toString() {
        return "Task1{" +
                "x=" + x +
                ", y=" + y +
                '}';
    }

    public int findMaxXY() {
        if (x >= y) {
            return x;
        } else {
            return y;
        }
    }

    public int sum() {
        return x + y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
}
