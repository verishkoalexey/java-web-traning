package by.traning.module0.simpleObjects.task3;

public class Application {
    public static void main(String[] args) {
        Student[] students = new Student[]{
                new Student("Алексеев И.С.", 42, new int[]{9, 7, 10, 8, 5}),
                new Student("Веришко Л.С.", 43, new int[]{10, 9, 9, 8, 10}),
                new Student("Клепик К.Р.", 42, new int[]{10, 8, 9, 9, 9}),
                new Student("Кухальский П.П.", 24, new int[]{10, 7, 10, 6, 7}),
                new Student("Шелкунович В.А.", 12, new int[]{4, 7, 5, 6, 5}),
                new Student("Приходько К.А.", 42, new int[]{9, 9, 9, 9, 9}),
                new Student("Тишевич В Е.", 12, new int[]{10, 10, 9, 10, 9}),
                new Student("Дудь Р.К.", 43, new int[]{9, 9, 6, 4, 5}),
                new Student("Супранович И.С", 24, new int[]{6, 8, 5, 6, 5}),
                new Student("Бычек К.Р.", 12, new int[]{2, 4, 2, 5, 10})
        };
        printExcellentStudents(students);

    }

    public static void printExcellentStudents(Student[] students) {
        System.out.println("List of Excellent Students:");
        boolean hasExcellentStudent = false;
        for (Student student : students) {
            if (student.isExcellentStudent()) {
                System.out.println("Student: "+ student.getSurnameInitials() + ", Group: " + student.getGroupNumber());
                hasExcellentStudent = true;
            }
        }
        if (!hasExcellentStudent) {
            System.out.println("List is empty.");
        }
    }
}
