package by.traning.module0.simpleObjects.task3;

public class Student {
    private String surnameInitials;
    private int groupNumber;
    private int[] progress;

    public Student(String surnameInitials, int groupNumber, int[] progress) {
        this.surnameInitials = surnameInitials;
        this.groupNumber = groupNumber;
        this.progress = progress;
    }

    public String getSurnameInitials() {
        return surnameInitials;
    }

    public int getGroupNumber() {
        return groupNumber;
    }

    public int[] getProgress() {
        return progress;
    }

    public boolean isExcellentStudent() {
        for (int grade : progress) {
            if (grade < 9) {
                return false;
            }
        }
        return true;
    }
}
